var labelFontSize = 15;

function Create2DArray(rows) {
	var arr = [];
	for (var i=0;i<rows;i++) {
		arr[i] = [];
	}
	return arr;
}

function Create2DMetaArray(rows) {
	var arr = [];
	for (var i = 0; i < rows; i++) {
		arr[i] = new meta(0, false, "");
	}
	return arr;
}

/* connects the last point drawn to the first point of that shape
   the default label is added when the shape is connected */
function completeShape(nameString) {
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = lineThickness * zoomDeg;
	if(pointCount > 2) {

		ctx.moveTo(shapesArray[shapesCount][pointCount-1][0],
			shapesArray[shapesCount][pointCount-1][1]);
		ctx.lineTo(shapesArray[shapesCount][0][0],
			shapesArray[shapesCount][0][1]);

    if(nameString == null) {
        nameString = "Shape" + shapesCount;
    }

    addLabel(shapesCount, nameString);
		ctx.stroke();
		pointCount = 0;
		shapesCount++;
	}
}


/* Add the label to the shape. The label defaults to the shape number
   Later on, I will add functionality to change the label to whatever
   you want */
function addLabel(shape, label) {
    // add the label to the metaArray information
    
    metaArray[shape].name = label + shape;

    var coords = findTopLeftCoordinate(shape);
    var x = coords[0];
    var y = coords[1];
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(x, y-15, label.length * 9, 14);
    ctx.fillStyle = "#000000";
    ctx.font = labelFontSize + "px Courier New";
    ctx.fillText(label, x, y-4);
}

function changeLabel(string) {
    var [shape, label] = string.split(';');
    addLabel(shape, label);
}

/* Find top left coordinate to place the label, prioritizing top over left
   We can use a different method for attaching the label later on (such as
   putting it in the center of the shape). I just don't know what will look
   the best, so this is how it will be for now. */
function findTopLeftCoordinate(shape) {
    var i = 0;
    var topLeft = i;
    var x, y;
    while(shapesArray[shape][i]) {
        // if higher than topLeft, set new topLeft
        if(shapesArray[shape][i][1] < shapesArray[shape][topLeft][1]) {
            topLeft = i;
        }
        // if equally as high as topLeft, see which is more left
        else if(shapesArray[shape][i][1] == shapesArray[shape][topLeft][1]) {
            if(shapesArray[shape][i][0] < shapesArray[shape][topLeft][0]) {
                topLeft = i;
            }
        }
        i++;
    }

    // hold x and y to save typing that long awful thing
    x = shapesArray[shape][topLeft][0];
    y = shapesArray[shape][topLeft][1];

    /* Checks if the label will run off the screen. If not, return top left
       coordinate. If so, return top left coordinate with adjustments */

    // if y-coordinate is too high
    if((y * zoomDeg) - (labelFontSize-5) <= 0) {
      y = y + labelFontSize - 5; 
    }
    
    /* if x-coordinate is too far right. the labelFontSize * 11 is an
       approximation of how long the label will be. We can make it more
       exact later on if needed. The 500 is how wide the canvas is */
    //alert("x: " + x + " eq: " + ((x * zoomDeg) + 50));
    if((x * zoomDeg) + 50 >= 500) {
      x = x - 50;
    }

    return [x, y];
}

function snapToGrid(x){
	if(x%(gridLength*zoomDeg) > ((gridLength/2)*zoomDeg))
		return x + (gridLength*zoomDeg) - x%(gridLength*zoomDeg);
	else
		return x - x%(gridLength*zoomDeg);
}




function zoom(inBool) {
    if(inBool) {
        //ctx.save();
        //ctx.scale(.9, .9);
        //ctx.restore();
        zoomDeg += .5;
    }
    else {
        //ctx.save();
        //ctx.scale(-.9, -.9);
        //ctx.restore();
        zoomDeg -= .5;
    }
    drawGrid();
    //drawAllShapes();
}

function calcAreas(){
    var i = 0;

    for(i; i < shapesCount; i++){


        let pointCount = shapesArray[i].length;
        var j, sum = 0, perim = 0;

        if(!(metaArray[i].hasCurves)){

            for(j = 0; j < pointCount; j++){

                sum +=      shapesArray[i][(j) % pointCount][0] * shapesArray[i][(j + 1) % pointCount][1]
                            - shapesArray[i][(j) % pointCount][1] * shapesArray[i][(j + 1) % pointCount][0];

                perim +=    hypotenuse(shapesArray[i][(j + 1) % pointCount][0] - shapesArray[i][(j) % pointCount][0],
                                        shapesArray[i][(j + 1) % pointCount][1] - shapesArray[i][(j) % pointCount][1]);
            }

            sum = sum / 2;
            sum = Math.abs(sum);
            sum = pixelsSquaredToGrid(sum, gridLength * zoomDeg);


            metaArray[i].setTotalArea(sum);
            metaArray[i].setPerimeter(perim);
            console.log(sum);
            console.log(perim);
            //alert("perimeter: " + perim + " area: " + sum);
        }
    }
}

function hypotenuse(delX, delY){
    x2 = Math.pow(delX, 2);
    y2 = Math.pow(delY, 2);

    return Math.pow(x2 + y2, .5);
}

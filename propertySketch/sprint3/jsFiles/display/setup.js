
//
// export class Setup {
//
//     constructor(){
//
//     }

function createTabs(){
	$(document).ready(function(){

        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        })

    })
}

function createButtons() {
    var load = $("<button></button>")
                .text("Load")
                .attr("id", "loadBtn")
                .attr("class", "control-button")
                .click(function(){
                    // legacyParse.ingest(prompt(
                    //                 "Enter legacy string: ",
                    //                 "R2, U4, ..."
                    //             ));

                    parse(prompt(
                                        "Enter legacy string: ",
                                        "R2, U4, ..."
                                    )
                                );
                });
    var save = $("<button></button>")
                .text("Save")
                .attr("id", "saveBtn")
                .attr("class", "control-button")
                .click(function(){
                    saveModel();
                });
    var update = $("<button></button>")
                .text("Calculate Area")
                .attr("id", "updateBtn")
                .attr("class", "control-button")
                .click(function(){
                    calcAreas();
                });

    var drawCurve = $("<button></button>")
                .text("Draw Curve")
                .attr("id","curveBtn")
                .attr("class","control-button")
                .click(function(){
					curveLine(
						prompt(
							"Enter length of curve: ",
							"20x30x10x20"
						)
					);
                });

	var drawCirc = $("<button></button>")
					.text("Draw Circle")
					.attr("id","drawCircBtn")
					.attr("class","control-button")
					.click(function(){
						drawCircle(
							prompt(
								"Enter dimensions (x,y,r,startangle,endangle) of circle: ",
								"20x30x10x20x10"
							)
					);

            });

    var drawRect = $("<button></button>")
                .text("Draw Rectangle")
                .attr("id","drawRectBtn")
                .attr("class","control-button")
                .click(function(){
                    drawRectangle(
						prompt(
                            "Enter dimensions of square: ",
                            "20x30"
                        )
                    );
                });

    var drawTri = $("<button></button>")
                .text("Draw Triangle")
                .attr("id","drawTriBtn")
                .attr("class","control-button")
                .click(function(){
                    drawTriangle(
						prompt(
                            "Enter height and length of triangle: ",
                            "20x30"
                        )
                    );
                });

    var changeLbl = $("<button></button>")
                .text("Change label")
                .attr("id","changeLabelBtn")
                .attr("class","changeLabel-button")
                .click(function(){
                    changeLabel(
						prompt(
                            "Enter shape number and label: ",
                            "0;Label text"
                        )
                    );
                });

    $("#ioControlGroup").append(load);
	$("#saveGroup").append(save);
    $("#sketchControlGroup").append(drawRect);
    $("#sketchControlGroup").append(drawTri);
	$("#sketchControlGroup").append(drawCirc);
    $("#sketchControlGroup").append(drawCurve);
    $("#sketchControlGroup").append(update);
    $("#sketchControlGroup").append(changeLbl);


}

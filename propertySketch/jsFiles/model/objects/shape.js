
const shapeObj = function(p0){
    this.metadata = new meta();

    this.metadata.setName("Shape" + (globalStateData.getNumShapes()+1));
    this.metadata.setColor(getRandomColor());

    this.lineList = [];
    this.pointList = [];

    this.pointList[0] = p0;

    this.numLines = 0;
    this.numPoints = 0;


    this.addStraightLine = function(point){
        this.lineList[this.numLines] = new lineObj(this.pointList[this.numPoints], point);
        checkIfAndSetMostestPoint(point.x, point.y);
        this.numLines++;
        this.numPoints++;
        this.pointList[this.numPoints] = point;
    }


    this.addCurvedLine = function(point, c1, c2){
        this.lineList[this.numLines] = new lineObj(this.pointList[this.numPoints], point, true, c1, c2);
        this.numPoints++;
        this.numLines++;
        this.pointList[this.numPoints] = point;
        // go to draw line mode
    }

    this.addSimpleCurvedLine = function(end){
        var start = this.pointList[this.numPoints];

        // make the y for both control points the same (cY)
        if(start.y > end.y) {
            var yAdd = start.y;
        }
        else {
            var yAdd = end.y;
        }
        var yAve = Math.abs(start.y+end.y)/2;
        var cY = Math.abs(start.x-end.x) + yAve - yAdd;

        var cX = Math.abs(start.x+end.x)/3;
        if(start.x < end.x) {
            globalStateData.setTempC1(new pointObj(cX+start.x, cY));
            globalStateData.setTempC2(new pointObj(end.x-cX, cY));
            var c1 = globalStateData.getTempC1();
            var c2 = globalStateData.getTempC2();
        }
        this.lineList[this.numLines] = new lineObj(start, end, true, c1, c2);
        this.numPoints++;
        this.numLines++;
        this.pointList[this.numPoints] = end;
        // go to draw line mode
    }

    this.addLastLine = function(){
        this.addStraightLine(this.pointList[0]);

        // Set starting point mode
		globalStateData.setStates([false, false, false, false, true, false, false, false]);
    }

    this.incrementAll = function(x, y){
        for(let i = 0; i < this.getNumPoints(); i++){
            this.getPoint(i).x += x;
            this.getPoint(i).y += y;
        }
    }

    this.movePoint = function(x, y, i){
        this.getLine((i + 1) % this.getNumLines()).getFirst().x += x;
        this.getLine((i + 1) % this.getNumLines()).getFirst().y += y;
        this.getLine(i).getSecond().x += x;
        this.getLine(i).getSecond().y += y;
    }
    this.moveC1 = function(x, y, i){
        this.getLine(i).getC1().x += x;
        this.getLine(i).getC1().y += y;
    }
    this.moveC2 = function(x, y, i){
        this.getLine(i).getC2().x += x;
        this.getLine(i).getC2().y += y;
    }

    // Does get line work in all cases? When drawing the rectangle,
    // it might return undefined in in the y coordinate,
    // even though pointList appeared to be correct
    this.getPoint = function(i){ return this.pointList[i]; }
    this.getNumPoints = function(){ return this.numPoints; }
    this.incrementNumPoints = function(){ this.numPoints++; }

    this.getLine = function(i){ return this.lineList[i]; }
    this.getNumLines = function(){ return this.numLines; }

    // this.draw = new objDrawer(context);
}

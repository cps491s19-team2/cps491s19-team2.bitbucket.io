
//
// export class Setup {
//
//     constructor(){
//
//     }

function createTabs(){
	$(document).ready(function(){

        $('ul.tabs li').click(function(){
            var tab_id = $(this).attr('data-tab');

            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');

            $(this).addClass('current');
            $("#"+tab_id).addClass('current');
        })

    })
}

function createButtons() {
    var load = $("<button></button>")
                .text("Load")
                .attr("id", "loadBtn")
                .attr("class", "control-button")
                .click(function(){
                    // legacyParse.ingest(prompt(
                    //                 "Enter legacy string: ",
                    //                 "R2, U4, ..."
                    //             ));

                    parse(prompt(
                                        "Enter legacy string: ",
                                        "R2, U4, ..."
                                    )
                                );
                });
    var save = $("<button></button>")
                .text("Save")
                .attr("id", "saveBtn")
                .attr("class", "control-button")
                .click(function(){
                    saveModel();
                });
    var update = $("<button></button>")
                .text("Update Graph [calcAreas()]")
                .attr("id", "updateBtn")
                .attr("class", "control-button")
                .click(function(){
                    calcAreas();
                });
    $("#ioControlGroup").append(load);
    $("#saveGroup").append(save);
    $("#sketchControlGroup").append(update);
}



function Create2DArray(rows) {
	var arr = [];
	for (var i=0;i<rows;i++) {
		arr[i] = [];
	}
	return arr;
}

function Create2DMetaArray(rows) {
	var arr = [];
	for (var i = 0; i < rows; i++) {
		arr[i] = new meta(0, false, "");
	}
	return arr;
}

// connects the last point drawn to the first point of that shape
function completeShape() {
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = lineThickness * zoomDeg;
	if(pointCount > 2) {
		ctx.moveTo(shapesArray[shapesCount][pointCount-1][0],
			shapesArray[shapesCount][pointCount-1][1]);
		ctx.lineTo(shapesArray[shapesCount][0][0],
			shapesArray[shapesCount][0][1]);
		ctx.stroke();
		pointCount = 0;
		shapesCount++;
	}
}

function snapToGrid(x){
	if(x%(gridLength*zoomDeg) > (25*zoomDeg))
		return x + (50*zoomDeg) - x%(50*zoomDeg);
	else
		return x - x%(50*zoomDeg);
}




function zoom(inBool) {
    if(inBool) {
        //ctx.save();
        //ctx.scale(.9, .9);
        //ctx.restore();
        zoomDeg += .5;
    }
    else {
        //ctx.save();
        //ctx.scale(-.9, -.9);
        //ctx.restore();
        zoomDeg -= .5;
    }
    drawGrid();
    //drawAllShapes();
}


function drawGrid(){
    // clear the canvas first for zooming purposes
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var i = gridLength * zoomDeg;
    ctx.strokeStyle = "#AAAAAA";
    ctx.lineWidth = lineThickness * zoomDeg;

    // horizontal lines
    while(i < canvas.height) {
        ctx.beginPath();
        ctx.moveTo(0, i);
        ctx.lineTo(canvas.width, i);
        ctx.stroke();
        i = i + gridLength * zoomDeg;
    }
    i = gridLength * zoomDeg;

    // vertical lines
    while(i < canvas.width) {
        ctx.beginPath();
        ctx.moveTo(i, 0);
        ctx.lineTo(i, canvas.height);
        ctx.stroke();
        i = i + gridLength * zoomDeg;
    }
}


// redraws all the shapes (right now for zooming purposes)
function drawAllShapes() {
    ctx.strokeStyle = "#000000";
    ctx.lineWidth = lineThickness * zoomDeg;
    var shape, point;
    // go through every shape
    for(shape = shapesCount; shape  >= 0; shape--) {
        // go through every point in that shape
        point = 0;
        alert(shapesArray[shapesCount].toString());
        while(shapesArray[x][y]) {
            alert(shapesArray[x]);
            ctx.beginPath();
            ctx.moveTo(shapesArray[x][y][0], shapesArray[x][y][1]);
            alert(shapesArray[x]);
            ctx.lineTo(shapesArray[x][y+1][0], shapesArray[x][y+1][1]);
            ctx.stroke();
            y++;
        }

        alert(metaArray[x].name);
    }
}

// draws lines, obviously
function drawLine(x,y) {
	shapesArray[shapesCount][pointCount] = [x, y];
	ctx.strokeStyle = "#000000";
	ctx.lineWidth = lineThickness * zoomDeg;
	ctx.beginPath();

	if(pointCount > 0) {
		ctx.moveTo(shapesArray[shapesCount][pointCount-1][0],
			shapesArray[shapesCount][pointCount-1][1]);
		ctx.lineTo(x,y);
		ctx.stroke();
	}
	pointCount++;
}

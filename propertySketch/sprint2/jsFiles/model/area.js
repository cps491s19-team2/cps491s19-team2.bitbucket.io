function calcAreas(){
    var i = 0;
    for(i; i < shapesArray.length; i++){

        let pointCount = shapesArray[i].length;
        var j, sum = 0;

        if(!(metaArray[i].hasCurves)){

            for(j = 0; j < pointCount; j++){

                sum +=      shapesArray[i][(j) % pointCount][0] * shapesArray[i][(j + 1) % pointCount][1]
                            - shapesArray[i][(j) % pointCount][1] * shapesArray[i][(j + 1) % pointCount][0];
            }

            sum = sum / 2;
            sum = Math.abs(sum);
            sum = pixelsSquaredToGrid(sum, 50 * zoomDeg);


            metaArray[i].totalArea = sum;
            console.log(sum);
        }
    }
}

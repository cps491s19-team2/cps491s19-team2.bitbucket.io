# cps491s19-team2.bitbucket.io

<pre><code>University of Dayton
Department of Computer Science
CPS 491 - Capstone II, Spring 2019
Instructor: Dr. Phu Phung
</code></pre>

<h1 id="capstoneiiproject">Capstone II Project</h1>

<h2 id="propertysketchhtml5implementation">Property Sketch HTML5 Implementation</h2>

<h2 id="teammembers">Team Members</h2>

<pre><code>1. Anna Duricy, duricya1@udayton.edu
2. Dan Allman, allmand2@udayton.edu
3. Chris Pydych, pydychc1@udayton.edu
</code></pre>

<h2 id="companymentors">Company Mentors</h2>

<pre><code>1.Dwayne Nickels
  Tyler Technologies
  1, Tyler Way, Moraine, OH 45439
</code></pre>

<h2 id="Demo">Demo</h2>
<iframe width="560" height="315" src="https://www.youtube.com/embed/E7mza8GTY8U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2 id="acknowledgments">Acknowledgments</h2>

<pre><code>We would like to thank Tyler Technologies, especially Dwayne Nickels, for sponsoring this project and allowing us to work
with them and learn from them.
</code></pre>


